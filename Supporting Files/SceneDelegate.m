//
//  SceneDelegate.m
//  anonymous-message-publishing
//
//  Created by Lele Niu on 2020/3/24.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <libkern/OSAtomic.h>
#import "SceneDelegate.h"
#import <Masonry/Masonry.h>

#define timeIntence 5.0//图片放大的时间

@interface SceneDelegate ()
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIButton *skipButton;
@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions {
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

    //使主窗口显示到屏幕的最前端
    [self.window makeKeyAndVisible];
    //获取中心点的xy坐标
    float y = [UIScreen mainScreen].bounds.size.height / 2;
    float x = [UIScreen mainScreen].bounds.size.width / 2;
    //创建展示动画的imageView
    self.imageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.imageView.center = CGPointMake(x, y);
    self.imageView.userInteractionEnabled = YES;
    self.imageView.image = [UIImage imageNamed:@"启动图.jpg"];
    [self.window addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.bottom.equalTo(self.window);
    }];
    [self.window bringSubviewToFront:self.imageView];
    //添加一个跳过按钮
    self.skipButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.skipButton.layer.cornerRadius = 12.5;
    self.skipButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.skipButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.skipButton.backgroundColor = [UIColor grayColor];
    [self.skipButton addTarget:self action:@selector(skipAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.window addSubview:self.skipButton];
    [self.skipButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@25);
        make.width.equalTo(@70);
        make.top.equalTo(self.window).offset(40);
        make.trailing.equalTo(self.window).offset(-40);
    }];
    //跳过按钮的倒计时
    __block int32_t timeOutCount = timeIntence;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        OSAtomicDecrement32(&timeOutCount);
        if (timeOutCount == 0) {
            NSLog(@"timersource cancel");
            dispatch_source_cancel(timer);
        }
        [self.skipButton setTitle:[NSString stringWithFormat:@"%d 跳过", timeOutCount + 1] forState:(UIControlStateNormal)];
    });
    dispatch_source_set_cancel_handler(timer, ^{
        NSLog(@"timersource cancel handle block");
    });
    dispatch_resume(timer);
    //图片放大动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:timeIntence];
    self.imageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5f, 1.5f);
    [UIView commitAnimations];
    //图片消失动画
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (timeIntence * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:2 animations:^{
            self.imageView.alpha = 0;
            self.skipButton.alpha = 0;
        }];
    });
    //所有动画完成，删除图片和按钮
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) ((timeIntence + 2.0) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.imageView removeFromSuperview];
        [self.skipButton removeFromSuperview];
    });
}

//点击跳过按钮，删除图片和按钮
- (void)skipAction:(UIButton *)sender {
    [self.imageView removeFromSuperview];
    [self.skipButton removeFromSuperview];
}

- (void)sceneDidDisconnect:(UIScene *)scene {
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene {
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene {
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene {
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


@end
