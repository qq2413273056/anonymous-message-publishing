//
// Created by Lele Niu on 2020/3/26.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "TabBarController.h"
#import "UIColor+Addition.h"
#import "TopicPageViewController.h"

@interface TabBarController()<UITabBarControllerDelegate>
@end

@implementation TabBarController {

    NSInteger preSelectedTabBarIndex;

    UIImage *homeDefault;
    UIImage *homeSelected;
    UIImage *post;
    UIImage *personalDefault;
    UIImage *personalSelected;
}


- (void)tabBar:(UITabBar *)tabBar willBeginCustomizingItems:(NSArray<UITabBarItem *> *)items {
    [self initTabBarImage];
    for (NSUInteger i = 0; i < [items count]; ++i) {
        UITabBarItem *item = items[i];
        UIImage *tabBarDefaultImage;
        UIImage *tabBarSelectedImage;
        switch (i) {
            case 0:
                tabBarDefaultImage = homeDefault;
                tabBarSelectedImage = homeSelected;
                break;
            case 1:
                tabBarDefaultImage = post;
                tabBarSelectedImage = post;
                break;
            case 2:
                tabBarDefaultImage = personalDefault;
                tabBarSelectedImage = personalSelected;
                break;
        }
        item.selectedImage = tabBarSelectedImage;
        item.image = tabBarDefaultImage;
        self.tabBar.tintColor = [UIColor myBlack];
    }
}


- (void)viewDidLoad {
    self.delegate = self;
    [self initTabBarImage];
    NSArray<UITabBarItem *> *items = self.tabBar.items;
    for (NSUInteger i = 0; i < [items count]; ++i) {
        UITabBarItem *tabBarItem = items[i];
        UIImage *tabBarDefaultImage;
        UIImage *tabBarSelectedImage;
        switch (i) {
            case 0:
                tabBarDefaultImage = homeDefault;
                tabBarSelectedImage = homeSelected;
                break;
            case 1:
                tabBarDefaultImage = post;
                tabBarSelectedImage = post;
                break;
            case 2:
                tabBarDefaultImage = personalDefault;
                tabBarSelectedImage = personalSelected;
                break;
        }
        tabBarItem.selectedImage = tabBarSelectedImage;
        tabBarItem.image = tabBarDefaultImage;
    }
    self.tabBar.tintColor = [UIColor myBlack];
    self.tabBar.backgroundColor = [UIColor myBackground];
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if (viewController == self.viewControllers[1]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TopicPageViewController *svc = [sb instantiateViewControllerWithIdentifier:@"TopicPage"];
        svc.hidesBottomBarWhenPushed = true;
        svc.modalPresentationStyle = UIModalPresentationFullScreen;
        [tabBarController.selectedViewController presentViewController:svc animated:true completion:nil];
        return false;
    }

    if (viewController == self.viewControllers[2]) {
        UINavigationController *tmp = viewController;
        [tmp popToRootViewControllerAnimated:true];
    }
    return true;
}

- (void)initTabBarImage {
    homeDefault = [UIImage imageNamed:@"homeDefault"];
    homeSelected = [UIImage imageNamed:@"homeSelected"];
    post = [UIImage imageNamed:@"post"];
    post = [post imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    personalDefault = [UIImage imageNamed:@"personalDefault"];
    personalSelected = [UIImage imageNamed:@"personalSelected"];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSInteger index = [self.tabBar.items indexOfObject:item];
    if (index != preSelectedTabBarIndex) {
        NSMutableArray *array = [NSMutableArray array];
        for (UIView *btn in self.tabBar.subviews) {
            if ([btn isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
                [array addObject:btn];
            }
        }
        //执行动画
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        //速度控制函数
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.repeatCount = 1;      //次数
        animation.duration = 0.25;       //时间
        animation.fromValue = @0.8F;   //伸缩倍数
        animation.toValue = @1.0F;     //结束伸缩倍数
        [array[index] addAnimation:animation forKey:nil];
    }
    preSelectedTabBarIndex = index;
}


@end
