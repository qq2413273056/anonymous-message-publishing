//
// Created by Lele Niu on 2020/3/31.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PersonModel : NSObject
@property(nonatomic) int id;
@property(nonatomic, strong) NSString *_Nonnull author;
@property(nonatomic, strong) NSString *profilePhotoUrl;

+ (void)initPerson:(void (^ __nullable)(PersonModel *))completion;

+ (PersonModel *_Nullable)findPerson;
@end
