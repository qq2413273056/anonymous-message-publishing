//
// Created by Lele Niu on 2020/3/31.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "PersonModel.h"
#import "SQLiteManager.h"
#import "AFHTTPSessionManager.h"


@implementation PersonModel {

}

+ (void)initPerson:(void (^ __nullable)(PersonModel *))completion {
    PersonModel *person = [self findPerson];
    if (person == nil) {//没有用户信息 ，初始化用户信息
        PersonModel *personModel = [[PersonModel alloc] init];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSDictionary *requestParams = @{@"key": @"410f45232326af59c47005ef35de7d69"};
        [manager GET:@"http://api.tianapi.com/txapi/cname/index" parameters:requestParams headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
            NSDictionary *responseMap = responseObject;
            NSString *nickName = responseMap[@"newslist"][0][@"naming"];
            personModel.author = nickName;
            personModel.profilePhotoUrl = [NSString stringWithFormat:@"https://hn216.api.yesapi.cn/?s=Ext.Avatar.Show&nickname=%@&size=100&app_key=AA4D2BF3AFA21DA346E4AE06A8309C8A&sign=AA4D2BF3AFA21DA346E4AE06A8309C8A", nickName];
            [self savePerson:personModel];
            if (completion) {
                completion(personModel);
            }
        }    failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
            NSLog(@"%@", error);
        }];
    }
}


+ (PersonModel *)findPerson {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *querySql = [NSString stringWithFormat:
            @"select id,profilePhotoUrl, author from person "];
    NSArray *array = [manager findData:querySql fields2Class:^NSObject *(NSMutableDictionary *keyValue) {
        PersonModel *model = [[PersonModel alloc] init];
        NSArray<NSString *> *keys = [[keyValue keyEnumerator] allObjects];
        for (int i = 0; i < [keys count]; ++i) {
            NSObject *value = [keyValue objectForKey:keys[i]];
            [model setValue:value forKey:keys[i]];
        }
        return model;
    }];
    return [array count] > 0 ? array[0] : nil;
}

+ (void)savePerson:(PersonModel *)model {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *saveSql = [NSString stringWithFormat:
            @"insert into person (id,profilePhotoUrl,author) "
            "values('%d','%@','%@')"
            , model.id, model.profilePhotoUrl, model.author];
    [manager saveData:saveSql];
}
@end
