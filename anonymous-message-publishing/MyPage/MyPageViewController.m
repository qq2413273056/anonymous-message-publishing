//
//  MyPageViewController.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "MyPageViewController.h"
#import "UIImageView+WebCache.h"
#import "AFHTTPSessionManager.h"
#import "View+MASAdditions.h"
#import "SQLiteManager.h"
#import "PersonModel.h"
#import "HomePageViewController.h"
#import "TimerTaskService.h"
#import "UIColor+Addition.h"
#import "FavoriteViewController.h"

@interface MyPageViewController ()

@end

@implementation MyPageViewController {
    UIImageView *iconView;
    UILabel *nickNameView;
    UILabel *collectionView;
    UILabel *personalDeclarationLabel;
    UISwitch *refreshSwitch;
}

- (void)loadView {
    [super loadView];

    self.navigationController.navigationBar.prefersLargeTitles = TRUE;
    self.navigationItem.title = @"我的";
    self.navigationController.navigationBar.largeTitleTextAttributes =
            @{NSForegroundColorAttributeName: [UIColor myBlack],
                    NSFontAttributeName: [UIFont systemFontOfSize:34]};
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithHex:0xf8f8f9];

    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 15)];
    self.tableView.tableHeaderView = tableHeaderView;
}



- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section {
    if (section == 3) {
        return 3;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView1 {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if (indexPath.section == 0) {
        [self initHeadView];
        [cell addSubview:iconView];
        [cell addSubview:nickNameView];
        [cell addSubview:personalDeclarationLabel];

        [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(cell.contentView).with.offset(16);
            make.height.width.equalTo(@(36));
        }];

        [nickNameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconView.mas_right).with.offset(8);
            make.top.equalTo(cell.contentView.mas_top).with.offset(24);
        }];

        [personalDeclarationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(cell).with.inset(16);
            make.top.equalTo(iconView.mas_bottom).with.offset(16);
        }];
    } else if (indexPath.section == 1) {
        UIView *view = [self makeRowUseTemplate:@"edit" text:@"我发表的"];
        [cell addSubview:view];

        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(cell.contentView).with.inset(0);
        }];
    } else if (indexPath.section == 2) {
        collectionView = (UILabel *) [self makeRowUseTemplate:@"favor" text:@"收藏列表"];
        [cell addSubview:collectionView];
        [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(cell.contentView).with.inset(0);
        }];
    } else if (indexPath.section == 3) {
        switch (indexPath.row) {
            case 2: {
                UIView *view = [self makeRowUseTemplate:@"like" text:@"兴趣偏好"];
                [cell addSubview:view];
                [view mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.right.bottom.equalTo(cell.contentView).with.inset(0);
                }];
            }
                break;
            case 0: {
                UIView *uiView = [self makeRowUseTemplate:@"location" text:@"定时刷新"];
                [cell addSubview:uiView];
                refreshSwitch = [[UISwitch alloc] init];
                refreshSwitch.hidden = TRUE;
                [self registerEvent4Switch];
                [cell addSubview:refreshSwitch];
                [uiView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.right.bottom.equalTo(cell.contentView).with.inset(0);
                }];

                [refreshSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.equalTo(@(50));
                    make.top.equalTo(cell.mas_top).with.inset(10);
                    make.right.equalTo(cell.mas_right).with.inset(10);
                }];
            }
                break;
            case 1: {
                UIView *uiView = [self makeRowUseTemplate:@"delete" text:@"清除缓存"];
                [cell addSubview:uiView];
                [uiView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.right.bottom.equalTo(cell.contentView).with.inset(0);
                }];
            }
                break;
            default:
                break;
        }
    }
    return cell;
}

- (void)registerEvent4Switch {
    TimerTaskService *timerTaskService = [TimerTaskService getInstance];

    [refreshSwitch addTarget:timerTaskService action:@selector(dealRefreshPage:) forControlEvents:UIControlEventValueChanged];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.prefersLargeTitles = TRUE;
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
}


- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            break;
        case 1://我发表的

        {
            HomePageViewController *homePageController = [[HomePageViewController alloc] init];
            homePageController.fromMyPublishPage = TRUE;
            [self showViewController:homePageController sender:nil];
        }
            break;
        case 2://收藏列表
        {
            HomePageViewController *homePageController = [[HomePageViewController alloc] init];
            homePageController.fromFavorPage = TRUE;
            [self.navigationController showViewController:homePageController sender:nil];
        }
            break;
        case 3:
            if (indexPath.row == 2) {//兴趣爱好
                FavoriteViewController *favoriteViewController = [[NSBundle mainBundle] loadNibNamed:@"FavoriteViewController" owner:nil options:nil].firstObject;
                [self.navigationController showViewController:favoriteViewController sender:nil];
            }
            if (indexPath.row == 0) {//定时获取设置

            }
            if (indexPath.row == 1) {//清除缓存
                SQLiteManager *manager = [SQLiteManager getSharedInstance];
                [manager clearAllData];
                [PersonModel initPerson:^(PersonModel *person) {
                    nickNameView.text = person.author;
                    NSURL *imageUrl = [[NSURL alloc] initWithString:[[person profilePhotoUrl] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
                    [iconView sd_setImageWithURL:imageUrl];
                }];
            }
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 170;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 16;

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}


- (void)initHeadView {
    iconView = [[UIImageView alloc] init];
    nickNameView = [[UILabel alloc] init];
    nickNameView.textColor = [UIColor myBlack];

    personalDeclarationLabel = [[UILabel alloc] init];
    personalDeclarationLabel.numberOfLines = 0;
    personalDeclarationLabel.textColor = [UIColor myBlack];

    PersonModel *person = [PersonModel findPerson];
    nickNameView.text = person.author;
    nickNameView.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    NSURL *imageUrl = [[NSURL alloc] initWithString:[[person profilePhotoUrl] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    [iconView sd_setImageWithURL:imageUrl];
    personalDeclarationLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    personalDeclarationLabel.text = @"Loading...";

    AFHTTPSessionManager *manager1 = [AFHTTPSessionManager manager];
    manager1.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    manager1.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager1 GET:@"https://api.sunweihu.com/api/yan/api.php" parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSString *str2 = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        personalDeclarationLabel.text = str2;
    }     failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (UIView *)makeRowUseTemplate:(NSString *)imageName text:(NSString *)text {
    UILabel *row = [[UILabel alloc] init];
    UIImage *uiImage = [UIImage imageNamed:imageName];
    UIImageView *image = [[UIImageView alloc] initWithImage:uiImage];
    [row addSubview:image];
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    label.textColor = [UIColor myBlack];
    [row addSubview:label];

    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(row);
        make.left.equalTo(image.mas_right).with.inset(8);
    }];

    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(row.mas_top).with.inset(14);
        make.bottom.equalTo(row.mas_bottom).with.inset(14);
        make.left.equalTo(row).with.inset(16);
    }];

    return row;
}
@end
