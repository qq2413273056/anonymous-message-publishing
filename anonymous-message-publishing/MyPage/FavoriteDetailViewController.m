//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "FavoriteDetailViewController.h"
#import "SQLiteManager.h"
#import "HomePageModel.h"
#import "JsonUtil.h"
#import "View+MASAdditions.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Addition.h"


static NSString *const COLLECTION_CELL = @"UICollectionViewCell";

@implementation FavoriteDetailViewController {
    NSArray<HomePageModel *> *favoriteList;
    UICollectionView *collectionView;
    HomePageModel *currentModel;
    UILabel *nodataView;
}
- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = self.subtopicName;
    [self initNodataView];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:COLLECTION_CELL];
    collectionView.backgroundColor = [UIColor grayColor];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    [self.view addSubview:collectionView];
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(self.view).inset(10);
    }];
    [self.view addSubview:nodataView];

    [nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view).offset(0);
    }];
    [self loadData];
}

- (void)initNodataView {
    nodataView = [[UILabel alloc] init];
    nodataView.backgroundColor = [UIColor colorWithHex:0x666666];
    NSString *message = @"空空如也～";

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:message];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [paragraphStyle setLineSpacing:12];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [message length])];
    nodataView.attributedText = attributedString;
}

- (void)loadData {
    SQLiteManager *sqlManager = [SQLiteManager getSharedInstance];
    NSString *querySql = [NSString stringWithFormat:
            @"select profilePhotoUrl, author, created_at, content, subject, pictures from favorite where subject='%@'", self.subtopicName];
    favoriteList = [sqlManager findData:querySql fields2Class:^NSObject *(NSMutableDictionary *keyValue) {
        HomePageModel *model = [[HomePageModel alloc] init];
        NSArray<NSString *> *keys = [[keyValue keyEnumerator] allObjects];
        for (int i = 0; i < [keys count]; ++i) {
            NSObject *value = [keyValue objectForKey:keys[i]];
            if ([keys[i] isEqualToString:@"pictures"]) {
                value = [JsonUtil json2array:value];
            }
            [model setValue:value forKey:keys[i]];
        }
        return model;
    }];
    if ([favoriteList count] > 0) {
        nodataView.hidden = TRUE;
    } else {
        nodataView.hidden = FALSE;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionViewParam numberOfItemsInSection:(NSInteger)section {
    if (collectionViewParam == collectionView) {
        return [favoriteList count];
    } else {
        return [currentModel.pictures count];
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionViewParam cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionViewParam == collectionView) {
        __kindof UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_CELL forIndexPath:indexPath];
        [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        currentModel = favoriteList[[indexPath row]];
        [self renderCell:cell byModel:currentModel];
        cell.layer.borderWidth = 2;
        cell.layer.borderColor = [[UIColor whiteColor] CGColor];
        return cell;
    } else {
        __kindof UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_CELL forIndexPath:indexPath];
        [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        cell.backgroundColor = [UIColor whiteColor];
        UIImageView *imageView = [[UIImageView alloc] init];

        NSMutableString *url = [[NSMutableString alloc] initWithString:currentModel.pictures[[indexPath row]]];
        [url replaceOccurrencesOfString:@"\\" withString:@"" options:1 range:NSMakeRange(0, url.length)];

        NSURL *urlWithString = [NSURL URLWithString:url];
        [imageView sd_setImageWithURL:urlWithString];
        [cell addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.equalTo(cell).inset(2);
        }];
        return cell;
    }

}

- (void)renderCell:(__kindof UICollectionViewCell *)cell byModel:(HomePageModel *)model {
    UIView *parentView = [[UIView alloc] init];

    UILabel *contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 0;
    contentLabel.text = model.content;
    contentLabel.font = [UIFont systemFontOfSize:14.0f];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *picturesCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    picturesCollection.dataSource = self;
    picturesCollection.delegate = self;
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 2;

    layout.sectionInset = UIEdgeInsetsMake(2, 2, 2, 2);
    [picturesCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:COLLECTION_CELL];


    [parentView addSubview:picturesCollection];
    [parentView addSubview:contentLabel];

    if ([model.pictures count] > 0) {
        [picturesCollection mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(parentView).offset(3);
            make.top.bottom.equalTo(parentView).inset(10);
            make.width.equalTo(@(150));
        }];
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(parentView).offset(3);
            make.top.equalTo(parentView).offset(10);
            make.width.equalTo(@(200));
        }];
    } else {
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.left.equalTo(parentView).inset(3);
            make.top.bottom.equalTo(parentView).inset(10);
        }];
    }

    [cell addSubview:parentView];
    [parentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(cell).inset(2);
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionViewParam layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionViewParam == collectionView) {
        UICollectionViewCell *cell = [collectionViewParam cellForItemAtIndexPath:indexPath];

        HomePageModel *model = favoriteList[[indexPath row]];
        CGFloat contentHeight = [model.content
                boundingRectWithSize:CGSizeMake(150, 0)
                             options:NSStringDrawingTruncatesLastVisibleLine
                          attributes:@{
                                  NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                          } context:nil].size.height;
        CGFloat picturesHeight = 157;
        if ([model.pictures count] == 0) {
            picturesHeight = 0;
        }
        return CGSizeMake(collectionView.bounds.size.width - 20, (picturesHeight > contentHeight ? picturesHeight : contentHeight) + 20);
    } else {//图片相关
        CGFloat baseWidth = 130;
        CGFloat baseHeight = 157;
        NSUInteger picturesCount = [currentModel.pictures count];
        NSUInteger lineCount = ceil(picturesCount / 2.0);
        CGFloat perHeight = (baseHeight - (lineCount - 1) * 10) / lineCount;
        CGFloat perWeight = 0;
        if (picturesCount < 3) {
            perWeight = baseWidth / picturesCount;
        } else {
            perWeight = (baseWidth - 10) / 2;
        }

        return CGSizeMake(perWeight, perHeight);
    }
}


@end