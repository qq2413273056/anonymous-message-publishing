//
// Created by Lele Niu on 2020/3/31.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "FavoriteViewController.h"
#import "View+MASAdditions.h"
#import "NSObject+Topic.h"
#import "UIImageView+WebCache.h"
#import "FavoriteDetailViewController.h"
#import "UIColor+Addition.h"
#import "TopicService.h"


static NSString *const HEAD_COLLECTION_VIEW_CELL = @"HeadCollectionViewCell";

static NSString *const CONTENT_COLLECTION_VIEW_CELL = @"ContentCollectionViewCell";

@implementation FavoriteViewController {
    __weak IBOutlet UICollectionView *headCollectionView;

    __weak IBOutlet UICollectionView *contentCollectionView;

    UILabel *nodataView;

    NSArray<Topic *> *myFavoriteTopicList;
    NSArray<Subtopic *> *myFavoriteSubTopicInTopic;
    NSString *currentTopicName;
}
- (void)viewDidLoad {
    [self initHeadCollectionView];
    [self initContentCollectionView];
    [self initNodataView];
}

- (void)loadView {
    [super loadView];
}

- (void)initNodataView {
    nodataView = [[UILabel alloc] init];
    nodataView.backgroundColor = [UIColor colorWithHex:0xf3f2f7];
    nodataView.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    nodataView.textColor = [UIColor lightGrayColor];
    nodataView.textAlignment = NSTextAlignmentCenter;
    [nodataView sizeToFit];
    NSString *message = @"空空如也～";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:message];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [paragraphStyle setLineSpacing:12];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [message length])];
    nodataView.attributedText = attributedString;
    [self.view addSubview:nodataView];
    [nodataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view).offset(0);
    }];
}

//MARK:- InitComponent

- (void)initContentCollectionView {
    contentCollectionView.dataSource = self;
    contentCollectionView.delegate = self;
    contentCollectionView.backgroundColor = [UIColor colorWithHex:0xf5f5f5];
    UICollectionViewFlowLayout *contentCollectionViewLayout = contentCollectionView.collectionViewLayout;
    contentCollectionViewLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 10);
    [contentCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CONTENT_COLLECTION_VIEW_CELL];
}

- (void)initHeadCollectionView {
    self.view.backgroundColor = [UIColor colorWithHex:0xfcfcfc];
    headCollectionView.dataSource = self;
    headCollectionView.delegate = self;
    [headCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:HEAD_COLLECTION_VIEW_CELL];
}


//MARK:- Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == headCollectionView) {
        return [myFavoriteTopicList count];
    }
    if (collectionView == contentCollectionView) {
        myFavoriteSubTopicInTopic = [[TopicService getInstance] findMyFavoriteSubTopicInTopic:currentTopicName];
        return [myFavoriteSubTopicInTopic count];
    }
    return 0;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (collectionView == headCollectionView) {
        return [self dealHeadCell:collectionView indexPath:indexPath];
    }
    if (collectionView == contentCollectionView) {
        return [self dealContentCell:collectionView indexPath:indexPath];
    }
    return [[UICollectionViewCell alloc] init];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == headCollectionView) {
        [self headCellClicked:collectionView indexPath:indexPath];
    }
    if (collectionView == contentCollectionView) {
        [self contentCellClicked:collectionView indexPath:indexPath];
    }
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == headCollectionView) {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        UILabel *label = cell.subviews[0];
        label.textColor = nil;
    }
    if (collectionView == contentCollectionView) {

    }

}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == headCollectionView) {
        [headCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:TRUE scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:headCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == headCollectionView) {
        Topic *topic = myFavoriteTopicList[[indexPath row]];
        CGFloat width = [topic.mainTopic
                boundingRectWithSize:CGSizeMake(0, 45)
                             options:NSStringDrawingUsesLineFragmentOrigin
                          attributes:@{
                                  NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                          } context:nil].size.width;
        return CGSizeMake(width + 20, 45);
    }

    if (collectionView == contentCollectionView) {
        return CGSizeMake(64 * 1.3, 92 * 1.3);
    }
    return CGSizeMake(0, 0);
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationItem.title = @"我的偏好";
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
    return;
}


//MARK:- Private

- (void)loadData {
    myFavoriteTopicList = [[TopicService getInstance] findMyFavoriteTopic];
    if ([myFavoriteTopicList count] > 0) {
        nodataView.hidden = true;
    } else {
        nodataView.hidden = false;
    }
    [headCollectionView reloadData];
}

- (__kindof UICollectionViewCell *)dealHeadCell:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:HEAD_COLLECTION_VIEW_CELL forIndexPath:indexPath];
    [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    Topic *topic = myFavoriteTopicList[[indexPath row]];

    UILabel *label = [[UILabel alloc] init];
    label.text = topic.mainTopic;

    UILabel *notice = [[UILabel alloc] init];
    notice.text = @"";
    notice.layer.backgroundColor = [[UIColor colorWithHex:0xe1473c] CGColor];
    notice.layer.cornerRadius = 5;

    [cell addSubview:label];
    [cell addSubview:notice];

    [notice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(cell.mas_right).inset(2);
        make.top.equalTo(cell.mas_top).inset(0);
        make.height.width.equalTo(@(10));
    }];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(cell).inset(0);
        make.height.equalTo(@(45));
    }];
    return cell;
}

- (__kindof UICollectionViewCell *)dealContentCell:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CONTENT_COLLECTION_VIEW_CELL forIndexPath:indexPath];
    [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    Subtopic *subtopic = myFavoriteSubTopicInTopic[[indexPath row]];

    UIView *parentView = [[UIView alloc] init];
    UILabel *label = [[UILabel alloc] init];
    UIImageView *imageView = [[UIImageView alloc] init];

    [parentView addSubview:label];
    [parentView addSubview:imageView];
    [cell addSubview:parentView];

    UIBezierPath *shadowPath = [UIBezierPath
            bezierPathWithRect:parentView.bounds];
    parentView.layer.masksToBounds = NO;
    parentView.layer.shadowColor = [UIColor blackColor].CGColor;
    parentView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    parentView.layer.shadowOpacity = 0.5f;
    parentView.layer.shadowPath = shadowPath.CGPath;

    [parentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(cell).inset(0);
    }];

    label.text = [subtopic title];
    label.font = [UIFont systemFontOfSize:10];
    label.backgroundColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(parentView).inset(0);
        make.height.equalTo(@30);
    }];

    [imageView sd_setImageWithURL:subtopic.imageURL];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.equalTo(label.mas_bottom).offset(0);
        make.left.right.bottom.equalTo(parentView).inset(10);
    }];

    UILabel *notice = [[UILabel alloc] init];
    notice.text = @"";
    notice.layer.backgroundColor = [[UIColor colorWithHex:0xe1473c] CGColor];
    notice.layer.cornerRadius = 3;


    [cell addSubview:notice];
    [notice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(cell.mas_right).inset(4);
        make.top.equalTo(cell.mas_top).inset(4);
        make.height.width.equalTo(@(6));
    }];

    cell.layer.borderWidth = 1;
    cell.backgroundColor = [UIColor colorWithHex:0xf5f6f8];
    cell.layer.borderColor = [[UIColor colorWithHex:0xfefefe] CGColor];
    return cell;
}

- (void)headCellClicked:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UILabel *label = cell.subviews[0];
    label.textColor = [UIColor colorWithHex:0xc4655e];

    currentTopicName = myFavoriteTopicList[[indexPath row]].mainTopic;
    [cell.subviews[1] setHidden:TRUE];
    [contentCollectionView reloadData];
}

- (void)contentCellClicked:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [cell.subviews[1] setHidden:TRUE];
    FavoriteDetailViewController *view = [[FavoriteDetailViewController alloc] init];
    __kindof UILabel *label = cell.subviews[0].subviews[0];
    view.subtopicName = label.text;
    // [self showViewController:view sender:nil];
}

@end
