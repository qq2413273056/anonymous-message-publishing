//
// Created by Lele Niu on 2020/3/30.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "SQLiteManager.h"
#import <sqlite3.h>

static NSString *const DB_NAME = @"sqlite.db";

static SQLiteManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation SQLiteManager {

}

+ (SQLiteManager *)getSharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

- (BOOL)createDB {
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
            [docsDir stringByAppendingPathComponent:DB_NAME]];
    BOOL isSuccess = YES;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:databasePath] == NO) {
        const char *dbPath = [databasePath UTF8String];
        if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
            char *errMsg;
            //表：收藏
            const char *sql_stmt =
                    "create table if not exists favorite ("
                    "id INTEGER primary key,"
                    "profilePhotoUrl text,"
                    "author text,"
                    "created_at INTEGER,"
                    "subject text,"
                    "content text,"
                    "pictures text"
                    ")";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                    != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"Failed to create table[favorite]");
            }
            //表：个人信息
            sql_stmt =
                    "create table if not exists person ("
                    "id INTEGER primary key,"
                    "profilePhotoUrl text,"
                    "author text"
                    ")";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                    != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"Failed to create table[person]");
            }
            //表：我发表的
            sql_stmt =
                    "create table if not exists my_publish ("
                    "id INTEGER primary key,"
                    "profilePhotoUrl text,"
                    "author text,"
                    "created_at INTEGER,"
                    "subject text,"
                    "content text,"
                    "pictures text"
                    ")";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                    != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"Failed to create table[my_publish]");
            }

            sqlite3_close(database);
            return isSuccess;
        } else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

- (BOOL)saveData:(NSString *)sql {
    sqlite3_reset(statement);
    const char *dbPath = [databasePath UTF8String];
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        const char *insert_stmt = [sql UTF8String];
        sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
        return sqlite3_step(statement) == SQLITE_DONE;
    }
    return NO;
}


- (NSArray *)findData:(NSString *)sql fields2Class:(NSObject *(^ __nullable)(NSMutableDictionary *))fields2Class {
    sqlite3_reset(statement);
    const char *dbPath = [databasePath UTF8String];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        const char *query_stmt = [sql UTF8String];
        if (sqlite3_prepare_v2(database,
                query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            //提取sql 中字段名
            NSArray<NSString *> *array = [sql componentsSeparatedByString:@","];
            NSMutableArray *fieldsName = [[NSMutableArray alloc] init];
            for (int j = 0; j < [array count]; ++j) {
                NSString *trimString = [array[j] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSRange to = [trimString rangeOfString:@" " options:NSBackwardsSearch];
                if (to.location > sql.length) {//找不到位置
                    [fieldsName addObject:[array[j] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
                    continue;
                }
                NSString *fieldName;
                if ([trimString containsString:@" from"] || [trimString containsString:@"from "]) {
                    to = [trimString rangeOfString:@" from"];
                    fieldName = [array[j] substringToIndex:to.location + 1];
                } else {
                    fieldName = [array[j] substringFromIndex:to.location];
                }

                [fieldsName addObject:[fieldName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            }
            //读取每行记录
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *keyValue = [[NSMutableDictionary alloc] init];
                for (int j = 0; j < [fieldsName count]; ++j) {
                    NSString *string = [[NSString alloc] initWithUTF8String:
                            (const char *) sqlite3_column_text(statement, j)];
                    [keyValue setObject:string forKey:fieldsName[j]];
                }
                //结果以key-value 返回到 block
                [results addObject:fields2Class(keyValue)];
            }
            return results;
        }
    }
    return results;
}

- (NSSet *)findData4Distinct:(NSString *)sql {
    sqlite3_reset(statement);
    const char *dbPath = [databasePath UTF8String];
    NSMutableSet *results = [[NSMutableSet alloc] init];
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        const char *query_stmt = [sql UTF8String];
        if (sqlite3_prepare_v2(database,
                query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            //读取每行记录
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *string = [[NSString alloc] initWithUTF8String:
                        (const char *) sqlite3_column_text(statement, 0)];
                [results addObject:string];
            }
            return results;
        }
    }
    return results;
}

- (void)clearAllData {
    sqlite3_reset(statement);
    const char *dbPath = [databasePath UTF8String];
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        // 清空表：收藏
        int result = sqlite3_prepare_v2(database, "delete from favorite", -1, &statement, nil);
        if (result == SQLITE_OK) {
            sqlite3_step(statement);
        }
        //清空表：个人信息
        result = sqlite3_prepare_v2(database, "delete from person", -1, &statement, nil);
        if (result == SQLITE_OK) {
            sqlite3_step(statement);
        }

        //清空表：我发表的的
        result = sqlite3_prepare_v2(database, "delete from my_publish", -1, &statement, nil);
        if (result == SQLITE_OK) {
            sqlite3_step(statement);
        }
    }
}

- (void)deleteData:(NSString *)sql {
    sqlite3_reset(statement);
    const char *dbPath = [databasePath UTF8String];
    if (sqlite3_open(dbPath, &database) == SQLITE_OK) {
        const char *query_stmt = [sql UTF8String];
        int result = sqlite3_prepare_v2(database, query_stmt, -1, &statement, nil);
        if (result == SQLITE_OK) {
            sqlite3_step(statement);
        }
    }
}
@end