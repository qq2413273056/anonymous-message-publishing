//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "DateFormatUtil.h"


@implementation DateFormatUtil {

}


+ (NSString *)formatDate:(NSString *)originDateStr {

    //用日历类获得当前的日期
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //创建datefomater
    NSDateFormatter *ft = [[NSDateFormatter alloc] init];

    //当天
    originDateStr = [self changeJsonStringToDateStr:originDateStr];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *originDate = [formatter dateFromString:originDateStr];
    if ([calendar isDateInToday:originDate]) {
        NSTimeInterval interval = [[[NSDate alloc] init] timeIntervalSinceDate:originDate];
        int intValue = [[NSNumber numberWithDouble:interval] intValue];
        if (intValue < 60) {
            return @"刚刚";
        }
        if (intValue < 3600) {
            return [NSString stringWithFormat:@"%d分钟前", intValue / 60];
        }
        return [NSString stringWithFormat:@"%d小时前", intValue / 3600];
    }
        //昨天
        if ([calendar isDateInYesterday:originDate]) {
            ft.dateFormat = @"昨天 HH:mm";
            return [ft stringFromDate:originDate];
        }

        //计算年度差值
        NSDateComponents *components = [calendar components:NSCalendarUnitYear fromDate:[NSDate now] toDate:originDate options:NSCalendarWrapComponents];

        //今年
        if (components.year == 0) {
            ft.dateFormat = @"MM-dd";
            return [ft stringFromDate:originDate];
        }

        //剩下的都是往年了
        ft.dateFormat = @"yyyy-MM-dd HH:mm";
        return [ft stringFromDate:originDate];

}

+ (NSString *)changeJsonStringToDateStr:(NSString *)string {
    //带有T的时间格式，是前端没有处理包含时区的，强转后少了8个小时，date是又少了8个小时，所有要加16个小时。
    NSString *str = [string stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSString *sss = [str substringToIndex:19];
    //    NSString *str1 =[str stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    NSDateFormatter *dateFromatter = [[NSDateFormatter alloc] init];
    [dateFromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [dateFromatter setTimeZone:timeZone];
    NSDate *date = [dateFromatter dateFromString:sss];
    NSDate *newdate = [[NSDate date] initWithTimeInterval:8 * 60 * 60 sinceDate:date];//
    NSDate *newdate1 = [[NSDate date] initWithTimeInterval:8 * 60 * 60 sinceDate:newdate];
    NSString *newstr = [[NSString stringWithFormat:@"%@", newdate1] substringToIndex:19];
    return newstr;
}

+ (NSDate *)changeStringToDate:(NSString *)string {
    string = [self changeJsonStringToDateStr:string];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [formatter dateFromString:string];
}

+ (NSDate *)getLocalDateFormatAnyDate:(NSDate *)anyDate {
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];//或GMT
    NSTimeZone *desTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [desTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate *destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}
@end
