//
// Created by Lele Niu on 2020/3/30.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SQLiteManager : NSObject {
    NSString *databasePath;
}
+ (SQLiteManager *_Nonnull)getSharedInstance;

- (BOOL)saveData:(NSString *_Nonnull)registerNumber name:(NSString *_Nonnull)name
      department:(NSString *_Nonnull)department year:(NSString *_Nonnull)year;

- (NSArray *_Nonnull)findByRegisterNumber:(NSString *_Nonnull)registerNumber;

- (BOOL)saveData:(NSString *_Nonnull)sql;

- (NSArray *_Nonnull)findData:(NSString *_Nonnull)sql fields2Class:(NSObject *_Nonnull (^ __nullable)(NSMutableDictionary *_Nonnull))fields2Class;

- (void)clearAllData;

- (void)deleteData:(NSString *_Nonnull)sql;

- (NSSet *_Nonnull)findData4Distinct:(NSString *_Nonnull)sql;
@end
