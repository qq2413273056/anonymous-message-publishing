//
//  ToastUtil.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/30.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (ShowToast);
- (void)showToast:(NSString *)message second:(double)second;
@end
