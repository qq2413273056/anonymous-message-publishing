//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IconfontUtil.h"
#import "TBCityIconFont.h"


@implementation IconFontUtil {

}


+ (TBCityIconInfo *)getTBCityIconInfo:(NSString *)unicodeText imageSize:(NSInteger)imageSize imageColor:(UIColor *)imageColor {
    return TBCityIconInfoMake(unicodeText, imageSize, imageColor);
}

+ (UIImage *)getUIImage:(TBCityIconInfo *)tBCityIconInfo {
    return [UIImage iconWithInfo:tBCityIconInfo];
}

+ (UIImageView *)getImageView:(UIImage *)image withFrame:(CGRect)rect {
    UIImageView *view = [[UIImageView alloc] init];
    view.image = image;
    view.frame = rect;
    return view;
}

+ (UIImage *)getUIImage:(NSString *)unicodeText withImageSize:(NSInteger)imageSize withImageColor:(UIColor *)imageColor {
    return [self getUIImage:[self getTBCityIconInfo:unicodeText imageSize:imageSize imageColor:imageColor]];
}

+ (UIImageView *)getImageView:(NSString *)unicodeText withImageSize:(NSInteger)imageSize withImageColor:(UIColor *)imageColor withFrame:(CGRect)Rect {
    return [self getImageView:[self getUIImage:unicodeText withImageSize:imageSize withImageColor:imageColor] withFrame:Rect];
}


@end
