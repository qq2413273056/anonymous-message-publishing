//
// Created by Lele Niu on 2020/3/28.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ReflectUtil : NSObject
+ (NSArray *)getProperties:(Class)clazz;
@end