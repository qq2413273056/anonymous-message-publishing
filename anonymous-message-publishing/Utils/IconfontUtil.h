//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TBCityIconInfo;
@class UIColor;
@class UIImage;
//星球图标
static NSString *const ICON_FONT_XING_QIU_1 = @"\U0000e60c";
static NSString *const ICON_FONT_XING_QIU_2 = @"\U0000e604";
//发布图标
static NSString *const ICON_FONT_FA_BU_1 = @"\U0000e6b1";
static NSString *const ICON_FONT_FA_BU_2 = @"\U0000e668";
static NSString *const ICON_FONT_FA_BU_3 = @"\U0000e763";
//个人中心
static NSString *const ICON_FONT_PERSON_1 = @"\U0000e60d";
static NSString *const ICON_FONT_PERSON_2 = @"\U0000e6ef";
static NSString *const ICON_FONT_PERSON_3 = @"\U0000e689";
//相册
static NSString *const ICON_FONT_PHOTO_ALBUM_1 = @"\U0000e7f1";
static NSString *const ICON_FONT_PHOTO_ALBUM_2 = @"\U0000e607";
static NSString *const ICON_FONT_PHOTO_ALBUM_3 = @"\U0000e611";
static NSString *const ICON_FONT_PHOTO_ALBUM_4 = @"\U0000e60b";
static NSString *const ICON_FONT_PHOTO_ALBUM_5 = @"\U0000e62c";
//收藏
static NSString *const ICON_FONT_FAVORITE = @"\U0000e670";
static NSString *const ICON_FONT_NO_FAVORITE = @"\U0000eca7";
//刷新
static NSString *const ICON_FONT_REFRESH = @"\U0000e609";


@interface IconFontUtil : NSObject {

}

+ (UIImage *)getUIImage:(NSString *)unicodeText withImageSize:(NSInteger)imageSize withImageColor:(UIColor *)imageColor;

+ (UIImageView *)getImageView:(NSString *)unicodeText withImageSize:(NSInteger)imageSize withImageColor:(UIColor *)imageColor withFrame:(CGRect)Rect;

@end