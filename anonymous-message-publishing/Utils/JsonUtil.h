//
// Created by Lele Niu on 2020/3/30.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JsonUtil : NSObject
+ (id)json2array:(NSString *)jsonString;

+ (NSString *)toJSONData:(id)theData;

+ (id)json2ArrayOrNSDictionary:(NSData *)jsonData;

+ (NSString *)dictionaryToJsonString:(NSDictionary *)dict;
@end
