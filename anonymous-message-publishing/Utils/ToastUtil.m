//
//  ToastUtil.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/30.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "ToastUtil.h"

@implementation UIViewController (ShowToast)

- (void)showToast:(NSString *)message second:(double)second {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    alert.view.backgroundColor = [UIColor blackColor];
    alert.view.alpha = 0.6;
    alert.view.layer.cornerRadius = 15;
    
    [self presentViewController:alert animated:true completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, second * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:true completion:nil];
    });
}

@end
