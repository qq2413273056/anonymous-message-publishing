//
// Created by Lele Niu on 2020/3/28.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <objc/runtime.h>
#import "ReflectUtil.h"


@implementation ReflectUtil {

}

+ (NSArray *)getProperties:(Class)clazz {
    unsigned int count;// 记录属性个数
    objc_property_t *properties = class_copyPropertyList(clazz, &count);
    NSMutableArray *mArray = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        objc_property_t property = properties[i];
        const char *cName = property_getName(property);
        NSString *name = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
        [mArray addObject:name];
    }
    return mArray.copy;
}

@end