//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DateFormatUtil : NSObject
+ (NSString *)formatDate:(NSString *)originDateStr;

+ (NSDate *)changeStringToDate:(NSString *)string;
@end