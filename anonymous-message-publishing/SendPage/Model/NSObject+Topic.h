//
//  NSObject+Topic.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Subtopic : NSObject
@property(nonatomic, strong) NSString * _Nullable title;
@property(nonatomic, strong) NSURL * _Nullable imageURL;

@end

NS_ASSUME_NONNULL_BEGIN

@interface Topic: NSObject

@property(nonatomic, strong) NSString *mainTopic;
@property(nonatomic, strong) NSArray<Subtopic *> *subtopic;

@end

NS_ASSUME_NONNULL_END
