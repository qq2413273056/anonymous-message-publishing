//
//  SendPageViewController.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/26.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendPageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (nonatomic, strong) NSString *topic;

@end

NS_ASSUME_NONNULL_END
