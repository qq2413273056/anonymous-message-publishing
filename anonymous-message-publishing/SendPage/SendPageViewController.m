//
//  SendPageViewController.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/26.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "SendPageViewController.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import "SendPageImageCollectionViewCell.h"
#import "ToastUtil.h"
#import "PersonModel.h"
#import "SendViewModel.h"
#import "HomePageViewController.h"
#import "HomePageService.h"
#import "TopicPageViewController.h"

@interface SendPageViewController () <CTAssetsPickerControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate>

@property(weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UIButton *addImageButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sendButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property(nonatomic, strong) UICollectionView *imageCollectionView;
@property(nonatomic, strong) UIView *backview;

@end

@implementation SendPageViewController

NSArray *imageArray;
NSDictionary *message;
SendViewModel *sendViewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    _topicLabel.text = _topic;
    _contentTextView.delegate = self;
    [self setUpContentTextView];
    [self.view addSubview:self.backview];
    [self.view sendSubviewToBack:self.backview];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)transformView:(NSNotification *)notification {
    NSValue *keyBoardBeginBounds=[[notification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
    NSValue *keyBoardEndBounds=[[notification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect  endRect=[keyBoardEndBounds CGRectValue];

    CGFloat deltaY = endRect.origin.y - beginRect.origin.y;
    if (deltaY < -200) {
        deltaY += 32;
    } else if (deltaY > 200){
        deltaY -= 32;
    }
    CGRect frame = self.bottomView.frame;
    [UIView animateWithDuration:0.25f animations:^{
        [self.bottomView setFrame:CGRectMake(frame.origin.x, frame.origin.y+deltaY, frame.size.width, frame.size.height)];
    }];
}

#pragma mark - Upload Data

- (IBAction)send:(id)sender {
    PersonModel *person = [PersonModel findPerson];
    NSString *author = [person author];
    __block NSArray *imageURLArray = [[NSArray alloc] init];
    sendViewModel = [[SendViewModel alloc] init];
    [sendViewModel imageToURL:imageArray callBack:^(NSString * _Nonnull imageURL, NSError * _Nonnull error) {
        if (error) {
            [self showToast:@"发送失败，请稍后再试" second:2.0];
        }
        if (imageURL != nil) {
            imageURLArray = [imageURLArray arrayByAddingObject:imageURL];
        }
        if (imageURLArray.count == imageArray.count) {
            if (author != nil) {
                message = [NSDictionary dictionaryWithObjectsAndKeys:author, @"author", self->_topic, @"subject", self->_contentTextView.text, @"content", imageURLArray, @"pictures", nil];
            } else {
            message = [NSDictionary dictionaryWithObjectsAndKeys:self->_topic, @"subject", self->_contentTextView.text, @"content", imageURLArray, @"pictures", nil];
            }
            [sendViewModel uploadData:message handler:^(NSURLResponse * _Nonnull response, id  _Nonnull responseObject, NSError * _Nonnull error) {
                if (error) {
                    [self showToast:@"发送失败，请稍后再试" second:2.0];
                } else {
                    [self saveDataToDB:responseObject];
                    imageArray = nil;
                    [self.navigationController popViewControllerAnimated:true];
                    TopicPageViewController *tvc = [self.navigationController.viewControllers objectAtIndex:0];
                    [tvc dismissViewControllerAnimated:true completion:nil];
                    UINavigationController *nc = [[((UITabBarController *)self.view.window.rootViewController) viewControllers] objectAtIndex:0];
                    HomePageViewController * hvc = [nc.viewControllers objectAtIndex:0];
                    [hvc refreshTabView];
                }
            }];
        }
    }];
}

- (void)saveDataToDB:(id)object {
    [[HomePageService sharedInstance] saveMyPublish2DB:object];
}

#pragma mark - TextView

- (void)textViewDidChange:(UITextView *)textView {
    [self setUpContentTextView];
}

- (void)setUpContentTextView {
    NSInteger length = _contentTextView.text.length;
    if (length <= 0 && imageArray.count <= 0) {
        [_sendButton setEnabled:false];
    } else if (length > 0 && length <= 200) {
        [_sendButton setEnabled:true];
    } else if (length > 200) {
        if (self.presentedViewController == nil) {
            [self showToast:@"内容不能超过200字，请重新输入" second:2.0];
            [_sendButton setEnabled:false];
        }
    }
}

#pragma mark - insert image

- (IBAction)chooseImage:(id)sender {
    [self callSystemAlbum];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(PHAsset *)asset {
    NSInteger max = 9;
    if (picker.selectedAssets.count >= max) {
        return false;
    }
    return true;
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    [picker dismissViewControllerAnimated:true completion:^{
        CGFloat lysize = [UIScreen mainScreen].scale;
        PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
        requestOptions.resizeMode = PHImageRequestOptionsResizeModeExact;
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        __block NSArray *imagearr = [[NSArray alloc] init];
        for (NSInteger i = 0; i < assets.count; i++) {
            PHAsset *asset = assets[i];
            CGSize size = CGSizeMake(asset.pixelWidth / lysize, asset.pixelHeight / lysize);

            [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeDefault options:requestOptions resultHandler:^(UIImage *_Nullable result, NSDictionary *_Nullable info) {
                imagearr = [imagearr arrayByAddingObject:result];
                imageArray = imagearr;
                [self->_imageCollectionView reloadData];
                [self->_sendButton setEnabled:true];
            }];
        }
        [self.backview addSubview:self.imageCollectionView];
    }];
}

- (void)callSystemAlbum {
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status != PHAuthorizationStatusAuthorized)return;
        dispatch_async(dispatch_get_main_queue(), ^{
            CTAssetsPickerController *assetsPicker = [[CTAssetsPickerController alloc] init];
            assetsPicker.delegate = self;
            assetsPicker.showsSelectionIndex = true;
            assetsPicker.assetCollectionSubtypes = @[@(PHAssetCollectionSubtypeSmartAlbumUserLibrary), @(PHAssetCollectionSubtypeAlbumRegular)];
            [self presentViewController:assetsPicker animated:true completion:nil];
        });
    }];
}

- (UIView *)backview {
    if (_backview == nil) {
        _backview = [[UIView alloc] init];
        _backview = [[UIView alloc] initWithFrame:CGRectMake(0, 260, self.view.bounds.size.width, 400)];
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [_backview addGestureRecognizer:tapGestureRecognizer];
    return _backview;
}

- (void)keyboardHide:(UITapGestureRecognizer*)tap{
    [_contentTextView resignFirstResponder];
}

#pragma mark - collection

- (UICollectionView *)imageCollectionView {
    if (_imageCollectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat margin = 5;
        CGFloat itemHW = (self.backview.bounds.size.width - 4 * margin) / 3 - margin;
        layout.minimumLineSpacing = margin;
        layout.minimumInteritemSpacing = margin;
        layout.itemSize = CGSizeMake(itemHW, itemHW);
        layout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);

        _imageCollectionView = [[UICollectionView alloc] initWithFrame:self.backview.bounds collectionViewLayout:layout];
        _imageCollectionView.delegate = self;
        _imageCollectionView.dataSource = self;
        _imageCollectionView.backgroundColor = [UIColor whiteColor];

        [_imageCollectionView registerClass:[SendPageImageCollectionViewCell class] forCellWithReuseIdentifier:@"collectionCell"];
    }

    return _imageCollectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SendPageImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    cell.sendImageView.image = imageArray[indexPath.row];

    return cell;
}

@end
