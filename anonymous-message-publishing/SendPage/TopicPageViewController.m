//
//  SendPageViewController.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "TopicPageViewController.h"
#import "MainTopicTableViewCell.h"
#import "SubtopicTableViewCell.h"
#import "NSObject+Topic.h"
#import "TopicViewModel.h"
#import "SendPageViewController.h"
#import "TopicService.h"
#import <SDWebImage/SDWebImage.h>

@interface TopicPageViewController () <UITableViewDelegate, UITableViewDataSource>
@property(weak, nonatomic) IBOutlet UITableView *mainTableView;
@property(weak, nonatomic) IBOutlet UITableView *subTableView;

@property NSArray<Topic *> *topics;

@end

@implementation TopicPageViewController

NSInteger selectIndex;
NSString *topic;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"选择话题"];
    selectIndex = 0;

    self.topics = [[TopicService getInstance] findTopic];
    [self.mainTableView reloadData];
    [self.subTableView reloadData];
    [self.mainTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:true scrollPosition:UITableViewScrollPositionNone];
}

- (IBAction)closePage:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (tableView == self.mainTableView) {
        MainTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTopicCell" forIndexPath:indexPath];
        cell.mainTopicTitleLabel.text = _topics[indexPath.row].mainTopic;
        return cell;
    }
    if (tableView == self.subTableView) {
        SubtopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubtopicCell" forIndexPath:indexPath];

        dispatch_async(dispatch_get_main_queue(), ^{
            cell.subtopicLabel.text = self->_topics[selectIndex].subtopic[indexPath.row].title;
            [cell.subtopicImageView sd_setImageWithURL:self->_topics[selectIndex].subtopic[indexPath.row].imageURL];
        });
        return cell;
    }
    return nil;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mainTableView) {
        return _topics.count;
    }
    if (tableView == self.subTableView) {
        return _topics[selectIndex].subtopic.count;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.mainTableView) {
        selectIndex = indexPath.row;
        [_subTableView reloadData];
    }
    if (tableView == self.subTableView) {
        topic = _topics[selectIndex].subtopic[indexPath.row].title;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SendPageViewController *svc = [sb instantiateViewControllerWithIdentifier:@"SendPage"];
        svc.topic = topic;
        [self.navigationController pushViewController:svc animated:true];
    }
}

@end
