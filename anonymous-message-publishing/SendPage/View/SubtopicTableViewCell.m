//
//  SubtopicTableViewCell.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "SubtopicTableViewCell.h"

@implementation SubtopicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.subtopicImageView.layer.cornerRadius = 10;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
