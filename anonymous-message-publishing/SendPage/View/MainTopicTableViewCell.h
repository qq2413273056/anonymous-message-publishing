//
//  MainTopicTableViewCell.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTopicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainTopicTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *labelLine;

@end

NS_ASSUME_NONNULL_END
