//
//  MainTopicTableViewCell.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "MainTopicTableViewCell.h"
#import "UIColor+Addition.h"

@implementation MainTopicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.labelLine.hidden = !selected;
    self.mainTopicTitleLabel.textColor = selected ? self.labelLine.backgroundColor : [UIColor red:153 green:153 blue:153 alpha:1.0];
}

@end
