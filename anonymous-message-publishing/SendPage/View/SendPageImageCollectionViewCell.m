//
//  SendPageImageCollectionViewCell.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/27.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "SendPageImageCollectionViewCell.h"

@implementation SendPageImageCollectionViewCell

- (UIImageView *)sendImageView {
    if (_sendImageView == nil) {
        _sendImageView = [[UIImageView alloc] init];
        _sendImageView.frame = self.bounds;
        _sendImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _sendImageView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.sendImageView];
        self.clipsToBounds = true;
    }
    return self;
}

@end
