//
//  SubtopicTableViewCell.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/25.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubtopicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *subtopicImageView;
@property (weak, nonatomic) IBOutlet UILabel *subtopicLabel;

@end

NS_ASSUME_NONNULL_END
