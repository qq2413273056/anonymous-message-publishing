//
//  SendPageImageCollectionViewCell.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/27.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendPageImageCollectionViewCell : UICollectionViewCell
@property(nonatomic, strong) UIImageView *sendImageView;
@end

NS_ASSUME_NONNULL_END
