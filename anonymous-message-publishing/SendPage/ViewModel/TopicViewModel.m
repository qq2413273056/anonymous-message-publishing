//
//  TopicViewModel.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/26.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "TopicViewModel.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/SDWebImage.h>

@implementation TopicViewModel

- (void)sendRequest:(void(^ __nullable)(NSArray *data))fetchTopics {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://mock.yonyoucloud.com/mock/4916/topic" parameters:nil headers:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSArray *topics = [responseObject objectForKey:@"data"];
             fetchTopics(topics);
         } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
}

- (void)loadTopics:(void(^)(NSArray<Topic *> *topics))callBack {
    [self sendRequest:^(NSArray *data) {
        NSArray<Topic *> * topicsData = [[NSArray alloc] init];
        for (int i = 0; i < data.count; i++) {
            Topic *topic = [[Topic alloc] init];
            topic.mainTopic = [data[i] valueForKey:@"name"];
            if([data[i] objectForKey:@"subtopic"] != nil) {
                NSArray *subtopics = [data[i] objectForKey:@"subtopic"];
                NSArray *subData = [[NSArray alloc] init];
                for(int j = 0; j < subtopics.count; j++) {
                    Subtopic *sub = [[Subtopic alloc] init];
                    sub.title = [subtopics[j] valueForKey:@"title"];
                    sub.imageURL = [NSURL URLWithString:[subtopics[j] valueForKey:@"imageURL"]];
                    subData = [subData arrayByAddingObject:sub];
                }
                topic.subtopic = subData;
            }
            topicsData = [topicsData arrayByAddingObject:topic];
        }
        callBack(topicsData);
    }];
}

@end
