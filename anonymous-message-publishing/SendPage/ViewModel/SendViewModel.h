//
//  SendViewModel.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/4/1.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendViewModel: NSObject
- (void)imageToURL:(NSArray *)imageArray callBack:(void(^)(NSString *imageURL, NSError *error))callBack;
- (void)uploadData:(NSDictionary *)message handler:(void(^)(NSURLResponse *response, id responseObject, NSError *error))handler;
@end

NS_ASSUME_NONNULL_END
