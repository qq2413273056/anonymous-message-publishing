//
//  TopicViewModel.h
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/3/26.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "NSObject+Topic.h"
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopicViewModel: NSObject
- (void)loadTopics:(void(^)(NSArray<Topic *> *topics))callBack;
@end

NS_ASSUME_NONNULL_END


