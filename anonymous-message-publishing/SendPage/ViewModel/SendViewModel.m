//
//  SendViewModel.m
//  anonymous-message-publishing
//
//  Created by Yidi Zhao on 2020/4/1.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "SendViewModel.h"
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@implementation SendViewModel

- (void)imageToURL:(NSArray *)imageArray callBack:(void(^)(NSString *imageURL, NSError *error))callBack {
    if (imageArray.count <= 0) {
        callBack(nil, nil);
    }
    NSString *uploadImageURL = @"https://hao.su/tu/update.php";
    for (int i = 0; i < imageArray.count; i++) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        formatter.dateFormat=@"yyyyMMddHHmmss";
        NSString *str=[formatter stringFromDate:[NSDate date]];
        NSString *fileName=[NSString stringWithFormat:@"%@.jpg",str];
        UIImage *image = imageArray[i];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.28);
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:uploadImageURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
        } error:nil];

        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        AFHTTPResponseSerializer *responseSer = [AFHTTPResponseSerializer serializer];
        responseSer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", nil];
        manager.responseSerializer = responseSer;
        NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                callBack(nil, error);
            } else {
                NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSString *imageURL = [str substringWithRange:NSMakeRange(17,str.length-19)];
                callBack(imageURL, nil);
            }
        }];
        [uploadTask resume];
    }
}

- (void)uploadData:(NSDictionary *)message handler:(void(^)(NSURLResponse *response, id responseObject, NSError *error))handler {
    NSString *url = @"http://13.230.85.161:3000/anonymous_messages";
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:message error:nil];

    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        handler(response, responseObject, error);
    }];
    [uploadTask resume];
}

@end
