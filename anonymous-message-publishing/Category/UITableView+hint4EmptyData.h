//
// Created by Lele Niu on 2020/4/3.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITableView (hint4EmptyData)
- (void)tableViewDisplayWitMsg:(NSString *)message ifNecessaryForRowCount:(NSUInteger)rowCount;
@end