//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Addition)

+ (UIColor *)myPrimary;

+ (UIColor *)myBackground;

+ (UIColor *)myBlack;

+ (UIColor *)myGrey;

+ (UIColor *)mySkyBlue;

//设置RGB颜色
+ (UIColor *)red:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;

//将颜色转换成RGB
+ (NSArray *)convertColorToRGB:(UIColor *)color;

//设置十六进制颜色
+ (UIColor *)colorWithHex:(NSInteger)hex;

+ (UIColor *)colorWithHexString:(NSString *)hexString;
@end