//
// Created by Lele Niu on 2020/4/3.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "UITableView+hint4EmptyData.h"
#import "UIColor+Addition.h"


@implementation UITableView (hint4EmptyData)


- (void)tableViewDisplayWitMsg:(NSString *)message ifNecessaryForRowCount:(NSUInteger)rowCount {

    if (rowCount == 0) {
        // Display a message when the table is empty
        // 没有数据的时候，UILabel的显示样式
        UILabel *messageLabel = [UILabel new];
        messageLabel.backgroundColor = [UIColor colorWithHex:0xf3f2f8];

        messageLabel.text = message;
        messageLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        messageLabel.textColor = [UIColor lightGrayColor];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        [messageLabel sizeToFit];

        self.backgroundView = messageLabel;
    } else {
        UILabel *label = self.backgroundView;
        label.text = @"";
    }
}
@end