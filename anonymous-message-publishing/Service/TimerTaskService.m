//
// Created by Lele Niu on 2020/4/2.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimerTaskService.h"
#import "MyPageViewController.h"
#import "HomePageViewController.h"


@implementation TimerTaskService {

    NSTimer *homePageTimer;
}

+ (TimerTaskService *)getInstance {
    static TimerTaskService *s_instance_dj_singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance_dj_singleton = (TimerTaskService *) [[super allocWithZone:nil] init];
    });

    return s_instance_dj_singleton;
}

- (id)copyWithZone:(NSZone *)zone {
    return [TimerTaskService getInstance];
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [TimerTaskService getInstance];
}

- (void)dealRefreshPage:(id)dealRefreshPage {
    UISwitch *refreshSwitch = dealRefreshPage;
    UITableViewCell *cell = [refreshSwitch parentFocusEnvironment];
    UITableView *tableView = [cell parentFocusEnvironment];
    MyPageViewController *myPageViewController = tableView.dataSource;
    UITabBarController *barController = myPageViewController.tabBarController;
    __kindof UINavigationController *homePageNavigationController = barController.viewControllers[0];
    __kindof HomePageViewController *homePageViewController = [homePageNavigationController viewControllers][0];
    if ([refreshSwitch isOn]) {//开启刷新
        homePageTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:homePageViewController selector:@selector(doRefreshTabView) userInfo:nil repeats:YES];
    } else {
        [homePageTimer invalidate];
    }

}
@end