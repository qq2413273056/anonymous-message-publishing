//
// Created by Lele Niu on 2020/4/2.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TimerTaskService : NSObject
+ (TimerTaskService *)getInstance;

- (void)dealRefreshPage:(id)dealRefreshPage;
@end