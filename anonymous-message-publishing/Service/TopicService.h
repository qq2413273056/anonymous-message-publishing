//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Topic;
@class Subtopic;


@interface TopicService : NSObject
+ (TopicService *)getInstance;

- (NSArray<Topic *> *)findTopic;

- (Topic *)findTopicByTopicName:(NSString *)topicName;

- (NSArray<Subtopic *> *)findSubtopics;

- (Subtopic *)findSubtopicBySubtopicName:(NSString *)subtopicName;

- (NSArray<Subtopic *> *)findSubtopicsByParentTopicName:(NSString *)parentTopicName;

- (Topic *)findTopicBySubtopicName:(NSString *)subtopicName;

- (NSArray<NSString *> *)findMyFavoriteSubTopicStr;

- (NSArray<Subtopic *> *)findMyFavoriteSubTopic;

- (NSArray<Topic *> *)findMyFavoriteTopic;

- (NSArray<NSString *> *)findMyFavoriteTopicStr;

- (NSArray<Subtopic *> *)findMyFavoriteSubTopicInTopic:(NSString *)topicName;

- (NSMutableDictionary<NSString *, Topic *> *)mainTopicStr2mainTopicDic;

- (void)reloadData;
@end