//
// Created by Lele Niu on 2020/4/1.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "TopicService.h"
#import "AFHTTPSessionManager.h"
#import "NSObject+Topic.h"
#import "ReflectUtil.h"
#import "SQLiteManager.h"

@interface TopicService()
@property(strong, nonatomic) NSMutableDictionary<NSString *, Topic *> *mainTopicStr2mainTopicDic;
@property(strong, nonatomic) NSMutableDictionary<NSString *, Subtopic *> *subtopicStr2subtopicDic;
@property(strong, nonatomic) NSMutableDictionary<NSString *, NSString *> *subTopicStr2mainTopicStrDic;
@property(strong, nonatomic) NSMutableArray<Topic *> *topicList;
@property(strong, nonatomic) NSMutableArray<Subtopic *> *subtopicList;
@end


@implementation TopicService


- (NSArray<Topic *> *)findTopic {
    return _topicList;
}

- (Topic *)findTopicByTopicName:(NSString *)topicName {
    return _mainTopicStr2mainTopicDic[topicName];
}

- (NSArray<Subtopic *> *)findSubtopics {
    return _subtopicList;
}

- (Subtopic *)findSubtopicBySubtopicName:(NSString *)subtopicName {
    return _subtopicStr2subtopicDic[subtopicName];
}

- (NSArray<Subtopic *> *)findSubtopicsByParentTopicName:(NSString *)parentTopicName {
    Topic *topic = [self findTopicByTopicName:parentTopicName];
    return topic.subtopic;
}

- (Topic *)findTopicBySubtopicName:(NSString *)subtopicName {
    NSString *parentTopicName = _subTopicStr2mainTopicStrDic[subtopicName];
    return [self findTopicByTopicName:parentTopicName];
}

- (void)reloadData {
    [self loadData];
}

- (NSArray<NSString *> *)findMyFavoriteSubTopicStr {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    SQLiteManager *sqlManager1 = [SQLiteManager getSharedInstance];
    NSString *querySql2 = [NSString stringWithFormat:
            @"select  DISTINCT subject  from favorite"];
    return [[sqlManager1 findData4Distinct:querySql2] allObjects];
}

- (NSArray<Subtopic *> *)findMyFavoriteSubTopic {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray<NSString *> *subtopicStrList = [self findMyFavoriteSubTopicStr];
    for (int i = 0; i < [subtopicStrList count]; ++i) {
        if (subtopicStrList[i]) {
            [result addObject:[self findSubtopicBySubtopicName:subtopicStrList[i]]];
        }
    }
    return result;
}

- (NSArray<Topic *> *)findMyFavoriteTopic {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray<NSString *> *subTopicStrList = [self findMyFavoriteSubTopicStr];
    for (int i = 0; i < [subTopicStrList count]; ++i) {
        Topic *topic = [self findTopicBySubtopicName:subTopicStrList[i]];
        if (![result containsObject:topic]) {
            [result addObject:topic];
        }
    }
    return result;
}

- (NSArray<NSString *> *)findMyFavoriteTopicStr {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray<NSString *> *subTopicStrList = [self findMyFavoriteSubTopicStr];
    for (int i = 0; i < [subTopicStrList count]; ++i) {
        NSString *subjectStr = self.subTopicStr2mainTopicStrDic[subTopicStrList[i]];
        if (![result containsObject:subjectStr]) {
            [result addObject:subjectStr];
        }
    }
    return result;
}

- (NSMutableDictionary<NSString *, Topic *> *)mainTopicStr2mainTopicDic {
    return _mainTopicStr2mainTopicDic;
}


- (void)loadData {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *requestUrl = @"https://mock.yonyoucloud.com/mock/4916/topic";
    __weak typeof(self) weakSelf = self;
    [manager GET:requestUrl parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        NSArray<NSDictionary *> *dataTmp = responseObject[@"data"];
        weakSelf.mainTopicStr2mainTopicDic = [[NSMutableDictionary alloc] init];
        weakSelf.subTopicStr2mainTopicStrDic = [[NSMutableDictionary alloc] init];
        weakSelf.subtopicStr2subtopicDic = [[NSMutableDictionary alloc] init];
        weakSelf.subtopicList = [[NSMutableArray alloc] init];
        weakSelf.topicList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [dataTmp count]; ++i) {
            Topic *topic = [[Topic alloc] init];
            NSDictionary *dictionary = dataTmp[i];
            topic.mainTopic = dictionary[@"name"];
            NSArray *fields = [ReflectUtil getProperties:[Subtopic class]];
            NSArray *subtopics = dictionary[@"subtopic"];
            NSMutableArray *subtopicsTmp = [[NSMutableArray alloc] init];
            for (int k = 0; k < subtopics.count; ++k) {
                Subtopic *subtopic = [[Subtopic alloc] init];
                for (int j = 0; j < fields.count; ++j) {
                    [subtopic setValue:subtopics[k][fields[j]] forKey:fields[j]];
                }
                [subtopicsTmp addObject:subtopic];
                if (subtopic) {
                    weakSelf.subTopicStr2mainTopicStrDic[subtopic.title] = dictionary[@"name"];
                }
                [weakSelf.subtopicList addObject:subtopic];
                weakSelf.subtopicStr2subtopicDic[subtopic.title] = subtopic;
            }
            topic.subtopic = subtopicsTmp;
            weakSelf.mainTopicStr2mainTopicDic[topic.mainTopic] = topic;
            [weakSelf.topicList addObject:topic];
        }

    }    failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        NSLog(@"%@", error);
    }];

}

- (NSArray<Subtopic *> *)findMyFavoriteSubTopicInTopic:(NSString *)topicNameParam {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray<Subtopic *> *subTopic = [self findMyFavoriteSubTopic];
    for (int i = 0; i < [subTopic count]; ++i) {
        NSString *topicName = self.subTopicStr2mainTopicStrDic[subTopic[i].title];
        if ([topicName isEqualToString:topicNameParam]) {
            [result addObject:subTopic[i]];
        }
    }
    return result;
}

+ (TopicService *)getInstance {
    static TopicService *s_instance_dj_singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance_dj_singleton = (TopicService *) [[super allocWithZone:nil] init];
        [s_instance_dj_singleton loadData];
    });

    return s_instance_dj_singleton;
}

- (id)copyWithZone:(NSZone *)zone {
    return [TopicService getInstance];
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [TopicService getInstance];
}

@end
