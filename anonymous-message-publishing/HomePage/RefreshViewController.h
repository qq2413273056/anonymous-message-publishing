//
//  RefreshViewController.h
//  anonymous-message-publishing
//
//  Created by Lele Niu on 2020/3/27.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RefreshViewController : UIRefreshControl

@end

NS_ASSUME_NONNULL_END
