//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HomePageModel : NSObject
@property(nonatomic) int id;
@property(nonatomic, strong) NSString *profilePhotoUrl;
@property(nonatomic, strong) NSString *author;
@property(nonatomic, strong) NSString *created_at;
@property(nonatomic, strong) NSString *subject;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSMutableArray<NSString *> *pictures;


@property(nonatomic) BOOL willExpand;
@property(nonatomic) CGFloat contentHeight;
@property(nonatomic) CGFloat profilePhotoImageViewHeight;
@property(nonatomic) CGFloat imagesContainerViewHeight;
@property(nonatomic) BOOL hasCacheOfHeight;
@property(nonatomic) signed char isFavorite;

@end