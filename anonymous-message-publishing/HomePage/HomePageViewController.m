//
//  ViewController.m
//  anonymous-message-publishing
//
//  Created by Lele Niu on 2020/3/24.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "HomePageViewController.h"
#import "HomePageService.h"
#import "HomePageTableViewCell.h"
#import "HomePageModel.h"
#import "UIColor+Addition.h"
#import "UITableView+hint4EmptyData.h"
#import "MJRefreshNormalHeader.h"
#import "MJRefreshAutoNormalFooter.h"
#import "IconFontUtil.h"
#import "View+MASAdditions.h"


static NSString *const TABLE_CELL_IDENTIFIER = @"HomePageTableViewCell";
static NSString *const HOME_PAGE_TITLE = @"广场";

@interface HomePageViewController ()
@property(nonatomic, strong) HomePageService *homePageService;
@property(nonatomic, assign) NSInteger currentPageNumber;
@end

@implementation HomePageViewController
//MARK:- LifeCycle


- (void)loadView {
    [super loadView];

    [self setupContentView];

    [self initServiceAndLoadData];


}






// MARK:- viewSetup

- (void)setupContentView {
    self.tableView.estimatedRowHeight = 0;

    [self.tableView registerNib:[UINib nibWithNibName:TABLE_CELL_IDENTIFIER bundle:nil] forCellReuseIdentifier:TABLE_CELL_IDENTIFIER];

    [self initRefreshComponentAndAddEvent];
    [self initLoadDataComponent];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

//MARK:- InitComponent

- (void)initRefreshComponentAndAddEvent {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self refreshTabView];
    }];
}

- (void)initLoadDataComponent {
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadDataFromServer];
    }];
}

//MARK:- Delegate

- (void)refreshTabView {
    if ([self.dataList count] > 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    }
    [self refreshDataFromServer];
}

- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section {
    return [self.dataList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomePageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TABLE_CELL_IDENTIFIER];
    HomePageModel *homePageModel = self.dataList[(NSUInteger) [indexPath row]];
    [cell renderByModel:homePageModel];
    if (self.fromFavorPage) {
        [cell.favoriteButton setTitle:@"取消收藏" forState:UIControlStateSelected];
        [cell.favoriteButton setImage:[[UIImage alloc] init] forState:UIControlStateSelected];
        cell.favoriteButton.backgroundColor = [UIColor colorWithHex:0xf3f2f8];
        [cell.favoriteButton setTitleColor:[UIColor myGrey] forState:UIControlStateSelected];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell = [self tableView:self.tableView cellForRowAtIndexPath:indexPath];
    return [cell calculateCellHeight];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataList.count - 10) {
        [self loadDataFromServer];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.prefersLargeTitles = TRUE;
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
    self.navigationItem.title = HOME_PAGE_TITLE;
    self.navigationController.navigationBar.largeTitleTextAttributes =
            @{NSForegroundColorAttributeName: [UIColor myBlack],
                    NSFontAttributeName: [UIFont systemFontOfSize:34]};
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 15)];
    self.tableView.tableHeaderView = tableHeaderView;
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithHex:0xf8f8f9];

    if (self.fromMyPublishPage) {
        self.navigationItem.title = @"我发表的";
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
        return;
    }

    if (self.fromFavorPage) {
        self.navigationItem.title = @"我收藏的";
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
        return;
    }

    UIButton *refreshBtn = [[UIButton alloc] init];
    [refreshBtn setImage:[IconFontUtil getUIImage:ICON_FONT_REFRESH withImageSize:30 withImageColor:[UIColor myGrey]] forState:UIControlStateNormal];
    refreshBtn.layer.cornerRadius = 10;
    //设置边框颜色
    refreshBtn.layer.borderColor = [[UIColor colorWithHex:0xeeeeee] CGColor];
    //设置边框宽度
    refreshBtn.layer.borderWidth = 1.5f;
    refreshBtn.layer.masksToBounds = YES;
    [refreshBtn addTarget:self action:@selector(refreshTabView) forControlEvents:UIControlEventTouchDown];
    [self.navigationController.view addSubview:refreshBtn];
    [refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.navigationController.view).with.inset(10);
        make.bottom.equalTo(self.navigationController.view).with.inset(100);
        make.width.equalTo(@(35));
        make.height.equalTo(@(35));
    }];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}


//MARK:- Private

- (void)initServiceAndLoadData {
    self.homePageService = [HomePageService sharedInstance];
    [self refreshDataFromServer];
}

- (void)refreshDataFromServer {
    [self.tableView tableViewDisplayWitMsg:@"正在加载..." ifNecessaryForRowCount:[self.dataList count]];

    if (self.fromMyPublishPage) {
        self.dataList = [self.homePageService loadDataFromDB];
        [self.tableView tableViewDisplayWitMsg:@"空空如也～～" ifNecessaryForRowCount:[self.dataList count]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer setHidden:true];
        return;
    }

    if (self.fromFavorPage) {
        self.dataList = [self.homePageService loadFavorDataFromDB];
        [self.tableView tableViewDisplayWitMsg:@"空空如也～～" ifNecessaryForRowCount:[self.dataList count]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer setHidden:true];
        return;
    }

    self.currentPageNumber = 1;
    
    __weak typeof(self) weakSelf = self;
    [self.homePageService loadData:self.currentPageNumber++ complete:^(NSMutableArray *array) {
        self.dataList = array;
        [self.tableView tableViewDisplayWitMsg:@"空空如也～～" ifNecessaryForRowCount:[self.dataList count]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    }                    fail:^(NSError *error) {
        NSLog(@"%@", error);
        weakSelf.currentPageNumber--;
    }];
}

- (void)loadDataFromServer {
    __weak typeof(self) weakSelf = self;
    [self.homePageService loadData:self.currentPageNumber++ complete:^(NSMutableArray *array) {
        [self.dataList addObjectsFromArray:array];
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
    }                    fail:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        weakSelf.currentPageNumber--;
        NSLog(@"%@", error);
    }];
}


@end
