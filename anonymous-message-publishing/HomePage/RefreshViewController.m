//
//  RefreshViewController.m
//  anonymous-message-publishing
//
//  Created by Lele Niu on 2020/3/27.
//  Copyright © 2020 XXX. All rights reserved.
//

#import "RefreshViewController.h"

@implementation RefreshViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
}

- (void)willMoveToSuperview:(nullable UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
}


@end
