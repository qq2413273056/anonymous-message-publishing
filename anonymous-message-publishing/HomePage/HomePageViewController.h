//
//  ViewController.h
//  anonymous-message-publishing
//
//  Created by Lele Niu on 2020/3/24.
//  Copyright © 2020 XXX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageViewController : UITableViewController
@property(nonatomic) NSMutableArray *dataList;
@property(nonatomic) BOOL fromMyPublishPage;

@property(nonatomic) BOOL fromFavorPage;

- (void)refreshTabView;
@end
