//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomePageModel;


@interface HomePageService : NSObject
- (void)loadData:(NSInteger)currentPageNumber complete:(void (^ __nullable)(NSMutableArray *_Nullable))completion fail:(void (^ __nullable)(NSError *_Nullable))fail;

+ (HomePageService *_Nonnull)sharedInstance;

- (void)saveMyPublish2DB:(id _Nonnull)object;

- (NSMutableArray<HomePageModel *> *_Nonnull)loadDataFromDB;

- (NSArray<HomePageModel *> *_Nonnull)loadFavorDataFromDB;

- (BOOL)saveFavorData2DB:(HomePageModel *_Nonnull)homePageModel;

- (void)deleteFavorDataFromDB:(HomePageModel *_Nonnull)homePageModel;

- (BOOL)isFavorFor:(HomePageModel *_Nonnull)homePageModel;
@end
