//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "HomePageService.h"
#import "AFHTTPSessionManager.h"
#import "HomePageModel.h"
#import "ReflectUtil.h"
#import "SQLiteManager.h"
#import "JsonUtil.h"
#import "DateFormatUtil.h"


static NSString *const HOME_PAGE_DATA_URL = @"http://13.230.85.161:3000/anonymous_messages/query";

@implementation HomePageService {

}

- (void)loadData:(NSInteger)currentPageNumber complete:(void (^ __nullable)(NSMutableArray *))completion fail:(void (^ __nullable)(NSError *))fail {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSDictionary *requestParams = @{@"page": @(currentPageNumber)};
    NSString *requestUrl = [NSString stringWithFormat:@"%@", HOME_PAGE_DATA_URL];
    [manager GET:requestUrl parameters:requestParams headers:nil progress:nil success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        completion([self translateResponseBodyToObject:responseObject]);
    }    failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        fail(error);
    }];
}

- (NSMutableArray<HomePageModel *> *)translateResponseBodyToObject:(id)object {
    if ([object isKindOfClass:NSClassFromString(@"NSDictionary")]) {//TODO 调试
        return nil;
    }
    NSArray<NSDictionary *> *responseData = object;
    NSMutableArray<HomePageModel *> *array = [[NSMutableArray alloc] init];
    for (int i = 0; i < [responseData count]; ++i) {
        NSDictionary *dictionary = responseData[(NSUInteger) i];
        HomePageModel *homePageModel = [self dic2Model:dictionary];
        [array addObject:homePageModel];
    }
    return array;
}

- (HomePageModel *)dic2Model:(NSDictionary *)dictionary {
    HomePageModel *homePageModel = [[HomePageModel alloc] init];
    NSArray *clazzProperties = [ReflectUtil getProperties:[HomePageModel class]];
    for (int j = 0; j < [clazzProperties count]; ++j) {
        id propertyName = clazzProperties[(NSUInteger) j];
        if (dictionary[propertyName] && dictionary[propertyName] != [NSNull null]) {
            [homePageModel setValue:dictionary[propertyName] forKey:propertyName];
        }
    }
    if (homePageModel.author == nil) {
        homePageModel.author = @"未知";
    }
    if (homePageModel.profilePhotoUrl == nil) {
        homePageModel.profilePhotoUrl = [NSString stringWithFormat:@"https://hn216.api.yesapi.cn/?s=Ext.Avatar.Show&nickname=%@&size=100&app_key=AA4D2BF3AFA21DA346E4AE06A8309C8A&sign=AA4D2BF3AFA21DA346E4AE06A8309C8A", homePageModel.author];
    }
    return homePageModel;
}


+ (id)allocWithZone:(NSZone *)zone {
    return [HomePageService sharedInstance];
}

+ (HomePageService *)sharedInstance {
    static HomePageService *s_instance_dj_singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance_dj_singleton = (HomePageService *) [[super allocWithZone:nil] init];
    });

    return s_instance_dj_singleton;
}

- (id)copyWithZone:(NSZone *)zone {
    return [HomePageService sharedInstance];
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [HomePageService sharedInstance];
}

- (void)saveMyPublish2DB:(id)object {
    HomePageModel *model = [self dic2Model:object];
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *saveSql = [NSString stringWithFormat:
            @"insert into my_publish (id,profilePhotoUrl,author, created_at, subject,content,pictures) "
            "values('%d','%@','%@', '%@d', '%@', '%@', '%@')"
            , model.id, model.profilePhotoUrl, model.author, [DateFormatUtil changeStringToDate:model.created_at], model.subject, model.content, [JsonUtil toJSONData:model.pictures]];
    [manager saveData:saveSql];
}

- (NSMutableArray<HomePageModel *> *)loadDataFromDB {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *querySql = [NSString stringWithFormat:
            @"select profilePhotoUrl, author, created_at, content, subject, pictures from my_publish order by created_at desc"];
    NSArray *array = [manager findData:querySql fields2Class:^NSObject *(NSMutableDictionary *keyValue) {
        HomePageModel *model = [[HomePageModel alloc] init];
        NSArray<NSString *> *keys = [[keyValue keyEnumerator] allObjects];
        for (int i = 0; i < [keys count]; ++i) {
            NSObject *value = [keyValue objectForKey:keys[i]];
            if ([keys[i] isEqualToString:@"pictures"]) {
                value = [JsonUtil json2array:value];
            }
            [model setValue:value forKey:keys[i]];
        }
        return model;
    }];
    return array;
}

- (BOOL)saveFavorData2DB:(HomePageModel *)homePageModel {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *saveSql = [NSString stringWithFormat:
            @"insert into favorite (id,profilePhotoUrl,author, created_at, subject,content,pictures) "
            "values('%d','%@','%@', '%@d', '%@', '%@', '%@')"
            , homePageModel.id, homePageModel.profilePhotoUrl, homePageModel.author, [NSDate date], homePageModel.subject, homePageModel.content, [JsonUtil toJSONData:homePageModel.pictures]];
    return [manager saveData:saveSql];
}

- (NSArray<HomePageModel *> *)loadFavorDataFromDB {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *querySql = [NSString stringWithFormat:
            @"select id,profilePhotoUrl, author, created_at, content, subject, pictures from favorite "];
    return [manager findData:querySql fields2Class:^NSObject *(NSMutableDictionary *keyValue) {
        HomePageModel *model = [[HomePageModel alloc] init];
        NSArray<NSString *> *keys = [[keyValue keyEnumerator] allObjects];
        for (int i = 0; i < [keys count]; ++i) {
            NSObject *value = [keyValue objectForKey:keys[i]];
            if ([keys[i] isEqualToString:@"pictures"]) {
                value = [JsonUtil json2array:value];
            }
            [model setValue:value forKey:keys[i]];
        }
        return model;
    }];
}

- (BOOL)isFavorFor:(HomePageModel *)homePageModel {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *querySql = [NSString stringWithFormat:
            @"select profilePhotoUrl, author, created_at, content, subject, pictures from favorite where id='%d'", homePageModel.id];
    return [[manager findData:querySql fields2Class:^NSObject *(NSMutableDictionary *keyValue) {
        HomePageModel *model = [[HomePageModel alloc] init];
        NSArray<NSString *> *keys = [[keyValue keyEnumerator] allObjects];
        for (int i = 0; i < [keys count]; ++i) {
            NSObject *value = [keyValue objectForKey:keys[i]];
            if ([keys[i] isEqualToString:@"pictures"]) {
                value = [JsonUtil json2array:value];
            }
            [model setValue:value forKey:keys[i]];
        }
        return model;
    }] count] == 1;
}

- (void)deleteFavorDataFromDB:(HomePageModel *)homePageModel {
    SQLiteManager *manager = [SQLiteManager getSharedInstance];
    NSString *deleteSql = [NSString stringWithFormat:@"delete from favorite where id = %d", homePageModel.id];
    [manager deleteData:deleteSql];
}
@end
