//
// Created by Lele Niu on 2020/3/26.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface HomePageImageContainerView : UIView
- (CGFloat)calculateHeight;

- (void)loadImage:(NSArray<NSString *> *)array;
@end