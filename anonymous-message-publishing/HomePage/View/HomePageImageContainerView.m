//
// Created by Lele Niu on 2020/3/26.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "HomePageImageContainerView.h"
#import "SDAnimatedImageView+WebCache.h"
#import "View+MASAdditions.h"


@implementation HomePageImageContainerView {

}
- (CGFloat)calculateHeight {
    NSArray *array = self.subviews;
    NSUInteger numberOfCellOnRow = (array.count < 3) ? array.count : 3;
    CGFloat width = ([[UIScreen mainScreen] bounds].size.width - (numberOfCellOnRow + 1) * 15 - 10) / numberOfCellOnRow;
    if ([array count] == 0) {
        return 0;
    } else {
        NSInteger row = ([array count] - 1) / 3;
        return width * (row + 1) + (row + 1 + 1) * 10;
    }
}

- (void)loadImage:(NSArray<NSString *> *)array {
    if ([self.subviews count] == 0) {
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(0)).priorityHigh();
        }];
    }

    if ([self.subviews count] == array.count) {
        return;
    }
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    NSUInteger numberOfCellOnRow = (array.count < 3) ? array.count : 3;
    CGFloat width = ([[UIScreen mainScreen] bounds].size.width - (numberOfCellOnRow + 1) * 15 - 15) / numberOfCellOnRow;
    for (int j = 0; j < array.count; ++j) {
        UIImageView *view2 = [[UIImageView alloc] init];

        NSMutableString *url = [[NSMutableString alloc] initWithString:array[j]];
        [url replaceOccurrencesOfString:@"\\" withString:@"" options:1 range:NSMakeRange(0, url.length)];

        NSURL *urlWithString = [NSURL URLWithString:url];
        [(SDAnimatedImageView *) view2 sd_setImageWithURL:urlWithString];
        [self addSubview:view2];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            NSInteger row = (NSInteger) ceil(j / 3);
            NSInteger col = j % 3;
            MASViewAttribute *topAttribute = (row == 0) ? self.mas_top : self.subviews[(NSUInteger) ((row - 1) * 3 + col)].mas_bottom;
            MASViewAttribute *leftAttribute = (col == 0) ? self.mas_left : self.subviews[(NSUInteger) (j - 1)].mas_right;
            make.top.equalTo(topAttribute).with.inset(10);
            make.left.equalTo(leftAttribute).with.offset(col == 0 ? 0 : 10);
            make.width.equalTo(@(width));
            make.height.equalTo(@(width));
            if (array.count == 1) {
                //make.bottom.equalTo(self).with.inset(10);
            };
        }];
    }


}
@end