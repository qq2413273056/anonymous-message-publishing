//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class HomePageModel;


@interface HomePageTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *contentLabel;
@property(weak, nonatomic) IBOutlet UIImageView *profilePhotoImageView;
@property(weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property(weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property(weak, nonatomic) IBOutlet UILabel *topicLabel;
@property(weak, nonatomic) IBOutlet UIView *imagesContainerView;
@property(weak, nonatomic) IBOutlet UIButton *showAllButton;
@property(weak, nonatomic) IBOutlet UIButton *favoriteButton;


- (CGFloat)calculateCellHeight;

- (void)renderByModel:(HomePageModel *)model;
@end
