//
// Created by Lele Niu on 2020/3/25.
// Copyright (c) 2020 XXX. All rights reserved.
//

#import "HomePageTableViewCell.h"
#import "HomePageImageContainerView.h"
#import "View+MASAdditions.h"
#import "HomePageModel.h"
#import "UIImageView+WebCache.h"
#import "DateFormatUtil.h"
#import "PersonModel.h"
#import "UIColor+Addition.h"
#import "HomePageService.h"
#import "HomePageViewController.h"


@implementation HomePageTableViewCell {

    HomePageModel *homePageModel;
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x = 7.5;
    frame.size.width -= 2 * frame.origin.x;
    frame.size.height -= 2 * frame.origin.x;
    [super setFrame:frame];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10.0;
}

- (CGFloat)calculateCellHeight {
    CGFloat contentHeightTmp = 0;
    if (([self isOverflowOfContent] && homePageModel.willExpand) ||
            ![self isOverflowOfContent]) {
        contentHeightTmp = homePageModel.contentHeight;
    }
    if ([self isOverflowOfContent] && !homePageModel.willExpand) {
        contentHeightTmp = 150;
    }
    return 16 + homePageModel.profilePhotoImageViewHeight + 15 + contentHeightTmp + (!self.showAllButton.hidden ? 28 : 0) + homePageModel.imagesContainerViewHeight + 17 + 17 + 20;
}


- (BOOL)isOverflowOfContent {
    return homePageModel.contentHeight > 150;
}

- (IBAction)showAllContent:(id)sender {
    homePageModel.willExpand = !homePageModel.willExpand;
    UITableView *tableView = (UITableView *) self.superview;
    [tableView reloadRowsAtIndexPaths:@[[tableView indexPathForCell:self]] withRowAnimation:UITableViewRowAnimationNone];
}

- (IBAction)clickFavorite:(id)sender {
    HomePageService *service = [HomePageService sharedInstance];
    UIButton *favoriteButton = self.favoriteButton;
    homePageModel.isFavorite = !homePageModel.isFavorite;
    favoriteButton.selected = homePageModel.isFavorite;

    if (homePageModel.isFavorite) {//收藏
        [service saveFavorData2DB:homePageModel];
        favoriteButton.backgroundColor = [UIColor myPrimary];
    } else {//取消收藏
        [service deleteFavorDataFromDB:homePageModel];
        self.favoriteButton.backgroundColor = [UIColor colorWithHex:0xf3f2f8];

        if ([[favoriteButton titleForState:UIControlStateSelected] isEqualToString:@"取消收藏"]) {
            UITableView *tableView = self.superview;
            HomePageViewController *view = tableView.parentFocusEnvironment;
            NSIndexPath *path = [tableView indexPathForCell:self];
            [view.dataList removeObjectAtIndex:[path row]];
            [tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
            [view refreshTabView];
        }
    }


}

- (void)renderByModel:(HomePageModel *)homePageModelParam {
    homePageModel = homePageModelParam;

    [self initComponentsProperty];
    [self calculationComponentsHeightAndSaveCache];
    [self initButtonOfContentView];
    [self setupLayout];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (void)initButtonOfContentView {
    [self.showAllButton setTitle:homePageModel.willExpand ? @"收起" : @"全文" forState:UIControlStateNormal];
    if ([self isOverflowOfContent]) {
        self.showAllButton.hidden = FALSE;
    } else {
        self.showAllButton.hidden = TRUE;
    }
}


- (void)initComponentsProperty {
    NSURL *url = [[NSURL alloc] initWithString:[homePageModel.profilePhotoUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    [self.profilePhotoImageView sd_setImageWithURL:url];
    PersonModel *currentPerson = [PersonModel findPerson];
    if ([homePageModel.author isEqualToString:currentPerson.author]) {
        self.nickNameLabel.text = @"我";
    } else {
        self.nickNameLabel.text = homePageModel.author;
    }
    self.dateTimeLabel.text = [DateFormatUtil formatDate:homePageModel.created_at];
    self.topicLabel.text = homePageModel.subject;
    self.contentLabel.text = homePageModel.content;
    self.profilePhotoImageView.layer.cornerRadius = 18;

    [(HomePageImageContainerView *) self.imagesContainerView loadImage:homePageModel.pictures];


    UIButton *favoriteButton = self.favoriteButton;
    UIImage *favorImage = [UIImage imageNamed:@"favor"];

    [favoriteButton setTitle:@"1" forState:UIControlStateSelected];
    [favoriteButton setImage:[favorImage imageWithTintColor:[UIColor whiteColor]] forState:UIControlStateSelected];

    [favoriteButton setTitle:nil forState:UIControlStateNormal];
    [favoriteButton setImage:[favorImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];

    HomePageService *service = [HomePageService sharedInstance];
    BOOL isFavor = [service isFavorFor:homePageModel];
    homePageModel.isFavorite = isFavor;
    if (isFavor) {
        favoriteButton.backgroundColor = [UIColor myPrimary];
    } else {
        favoriteButton.backgroundColor = [UIColor colorWithHex:0xf3f2f8];
    }
    favoriteButton.selected = isFavor;
}


- (void)calculationComponentsHeightAndSaveCache {
    if (!homePageModel.hasCacheOfHeight) {
        homePageModel.contentHeight = [self.contentLabel.text
                boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width - 50, 0)
                             options:
                                     NSStringDrawingUsesLineFragmentOrigin

                          attributes:@{
                                  NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size:14]
                          } context:nil].size.height;

        homePageModel.profilePhotoImageViewHeight = [self.profilePhotoImageView frame].size.height;

        HomePageImageContainerView *view = (HomePageImageContainerView *) self.imagesContainerView;
        homePageModel.imagesContainerViewHeight = view.calculateHeight;

        homePageModel.hasCacheOfHeight = TRUE;
    }

}

- (void)setupLayout {
    if (!homePageModel.willExpand) {
        [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.lessThanOrEqualTo(@(150));
        }];
    } else {
        [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(homePageModel.contentHeight + 1));
        }];
    }


    [self.imagesContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (![self isOverflowOfContent]) {
            make.top.equalTo(self.contentLabel.mas_bottom).with.offset(0);
        } else {
            make.top.equalTo(self.contentLabel.mas_bottom).with.offset(30);
        }
    }];


}


@end
