require 'test_helper'

class AnonymousMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @anonymous_message = anonymous_messages(:one)
  end

  test "should get index" do
    get anonymous_messages_url, as: :json
    assert_response :success
  end

  test "should create anonymous_message" do
    assert_difference('AnonymousMessage.count') do
      post anonymous_messages_url, params: { anonymous_message: { author: @anonymous_message.author, content: @anonymous_message.content, subject: @anonymous_message.subject } }, as: :json
    end

    assert_response 201
  end

  test "should show anonymous_message" do
    get anonymous_message_url(@anonymous_message), as: :json
    assert_response :success
  end

  test "should update anonymous_message" do
    patch anonymous_message_url(@anonymous_message), params: { anonymous_message: { author: @anonymous_message.author, content: @anonymous_message.content, subject: @anonymous_message.subject } }, as: :json
    assert_response 200
  end

  test "should destroy anonymous_message" do
    assert_difference('AnonymousMessage.count', -1) do
      delete anonymous_message_url(@anonymous_message), as: :json
    end

    assert_response 204
  end
end
