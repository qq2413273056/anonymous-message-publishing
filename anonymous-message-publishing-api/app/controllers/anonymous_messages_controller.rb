class AnonymousMessagesController < ApplicationController
  before_action :set_anonymous_message, only: [:show, :update, :destroy]
  wrap_parameters format: :json

  # GET /anonymous_messages
  def index
    @anonymous_messages = AnonymousMessage.all

    render json: @anonymous_messages
  end

  def query
    page = params.fetch(:page, 1).to_i
    page_size = params.fetch(:page_size, 20).to_i
    @anonymous_messages = AnonymousMessage.all
    @anonymous_messages = @anonymous_messages.where(subject: params[:subject]) unless params[:subject].blank?
    @anonymous_messages = @anonymous_messages.offset((page - 1) * page_size)
    @anonymous_messages = @anonymous_messages.limit(page_size)
    render json: @anonymous_messages
  end

  # GET /anonymous_messages/1
  def show
    render json: @anonymous_message
  end

  # POST /anonymous_messages
  def create
    @anonymous_message = AnonymousMessage.new(anonymous_message_params)

    if @anonymous_message.save
      render json: @anonymous_message, status: :created, location: @anonymous_message
    else
      render json: @anonymous_message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /anonymous_messages/1
  def update
    if @anonymous_message.update(anonymous_message_params)
      render json: @anonymous_message
    else
      render json: @anonymous_message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /anonymous_messages/1
  def destroy
    @anonymous_message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_anonymous_message
      @anonymous_message = AnonymousMessage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def anonymous_message_params
      params.require(:anonymous_message).permit(:author, :subject, :content, pictures: [])
    end
end
