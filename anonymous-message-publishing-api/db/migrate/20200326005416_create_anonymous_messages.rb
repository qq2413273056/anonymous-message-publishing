class CreateAnonymousMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :anonymous_messages do |t|
      t.string :author
      t.string :subject
      t.text :content

      t.timestamps
    end
  end
end
