class AddPicturesToAnonymousMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :anonymous_messages, :pictures, :jsonb, default: []
  end
end
