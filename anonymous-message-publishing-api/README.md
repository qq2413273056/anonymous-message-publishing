REQUIREMENTS
------------

- docker-compose

INSTALLATION
------------

- build docker-image, ```docker-compose build```
- create and edit secrets ```docker-compose run web rails credentials:edit```
- create database, ```docker-compose run web rails db:create```
- migrate database,```docker-compose run web rails db:migrate```

Quick Start
------------

1. Run/Rebuild docker containers

```docker-compose up``` / ```docker-compose up --build```

2. Rebuild docker containers 

```docker-compose build```

CONFIGURATION
-----------

- Dockerfile
 container setup config. could add extra editor at line 2.
 
- docker-compose.yml
 container configs.
 - change environment ```edit web:environment can change environment```
 - change ports ```edit web:ports can change ports, default is 3000.```

ENVIRONMENT
-----------

- EDITOR
 text editor used when editing secrets, must be installed in Dockerfile.
 
- RAILS_MAX_THREADS
 database poll size, default is 5 when not given.